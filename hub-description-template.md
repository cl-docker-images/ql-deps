- [Supported Tags](#org16ff3e7)
  - [Simple Tags](#org0dc368f)
  - [Shared Tags](#org9a399d2)
- [Quick Reference](#org813d456)
- [What is Quicklisp?](#orgb3374ac)
- [What's in the image?](#orgd8b940f)
- [License](#org3ecc5c0)



<a id="org16ff3e7"></a>

# Supported Tags


<a id="org0dc368f"></a>

## Simple Tags

INSERT-SIMPLE-TAGS


<a id="org9a399d2"></a>

## Shared Tags

INSERT-SHARED-TAGS


<a id="org813d456"></a>

# Quick Reference

-   **Quicklisp Home Page:** <https://beta.quicklisp.org>
-   **Where to file Docker image related issues:** <https://gitlab.common-lisp.net/cl-docker-images/ql-deps>
-   **Maintained by:** [Eric Timmons](https://github.com/daewok)
-   **Supported platforms:** `linux/amd64`, `linux/arm64/v8`, `linux/arm/v7`


<a id="orgb3374ac"></a>

# What is Quicklisp?

From [Quicklisp's Home Page](https://beta.quicklisp.org):

> Quicklisp is a library manager for Common Lisp. It works with your existing Common Lisp implementation to download, install, and load any of over 1,500 libraries with a few simple commands.


<a id="orgd8b940f"></a>

# What's in the image?

This image contains the Quicklisp installer, located at `/usr/local/share/common-lisp/source/quicklisp/quicklisp.lisp`, as well as the dependencies needed to load any library in the primary Quicklisp distribution. This list of packages is adapted from [debian-9-packages.txt](https://github.com/quicklisp/quicklisp-controller/blob/master/debian-setup/debian-9-packages.txt). PRs welcome to add anything that you find is missing.

Only the installer is included as the installed version of Quicklisp does not lend itself well to flexible Dockerization. There are two primary reasons for this. First, installing Quicklisp also downloads the current package metadata. This can cause images to age poorly or have otherwise unexpected behavior based on when the image was last built. Second, there is no option to install Quicklisp in a non user-writeable location. This results in surprises when UIDs don't match up.


<a id="org3ecc5c0"></a>

# License

The Quicklisp installer is licensed under the [MIT License](https://github.com/quicklisp/quicklisp-bootstrap/blob/master/LICENSE.txt).

The Dockerfiles used to build the images are licensed under BSD-2-Clause.

As with all Docker images, these likely also contain other software which may be under other licenses (such as Bash, etc from the base distribution, along with any direct or indirect dependencies of the primary software being contained).

As for any pre-built image usage, it is the image user's responsibility to ensure that any use of this image complies with any relevant licenses for all software contained within.
