#
# NOTE: THIS DOCKERFILE IS GENERATED VIA "update.sh"
#
# PLEASE DO NOT EDIT IT DIRECTLY.
#
FROM buildpack-deps:buster

RUN set -x \
    && package_list="openjdk-11-jdk-headless gosu \
    swig libopenmpi-dev libusb-dev libpython2.7-dev cvs screen build-essential \
    libsdl1.2debian libsdl-image1.2 libsdl-gfx1.2-dev libsdl-mixer1.2-dev libuv1-dev \
    libpython2.7 libgtkglext1-dev libgtk2.0-dev libglu1-mesa freeglut3-dev \
    libdrm-dev libgbm-dev libegl1-mesa-dev libsdl2-2.0-0 libhdf5-dev libblas-dev liblapack-dev \
    libfixposix-dev libgtk2.0-0 libgtk-3-0 \
    libdevil-dev libenchant-dev libev-dev libfam-dev libfcgi-dev libfbclient2 libfreeimage-dev \
    libfuse-dev libgeoip-dev libgeos-dev libgit2-dev libftgl2 libglu1-mesa-dev libgl1-mesa-dev \
    libglfw3-dev libglfw3 libgraphviz-dev libkyotocabinet-dev liballegro-acodec5-dev \
    liballegro-audio5-dev liballegro-dialog5-dev liballegro-image5-dev liballegro-physfs5-dev liballegro5-dev \
    liballegro-ttf5-dev libpuzzle1 liblinear-dev libsvm3 \
    libblas3 liblapack3 freetds-dev ocl-icd-opencl-dev libode-dev libopenal-dev libalut-dev libfann-dev \
    libglpk-dev libgsl-dev libpango1.0-dev libplplot-dev libportaudio2 libproj-dev pslib1 \
    librabbitmq-dev r-mathlib librrd-dev libsane-dev libsdl2-image-2.0-0 \
    libsdl2-ttf-2.0-0 libsoil-dev libsdl1.2-dev libtesseract-dev libtidy-dev libtokyocabinet-dev libwayland-dev \
    libsoup2.4-1 libwebkit2gtk-4.0-dev libxkbcommon-dev libassimp-dev libsdl-mixer1.2 \
    unixodbc-dev libncursesw5 libxrandr-dev libbluetooth-dev libsdl2-gfx-1.0-0 \
    libev4 libleveldb-dev libnet1-dev libsdl-ttf2.0-0 liblmdb-dev libmagic-dev libflac8 libasound2 \
    libmpg123-0 libvorbisfile3 libpcap0.8-dev gramps r-base-core libqt4-dev \
    libsnappy-dev" \
    && case "$(dpkg --print-architecture)" in \
    amd64) package_list="$package_list primus-libs";; \
    esac \
# Differences from Quicklisp's package list (besides trivial updates and
# removing things already in buildpack-deps)
# https://github.com/quicklisp/quicklisp-controller/blob/master/debian-setup/debian-9-packages.txt
# libgfortran-8-dev replaces libgfortran3
#
# Removed: triplea libsmokeqt4-dev libsmokeqtgui4-3 emacs-nox libsqlite0-dev
    && apt-get update \
    && apt-get install -y --no-upgrade --no-install-recommends $package_list \
    && rm -rf /var/lib/apt/lists/*

# Add the Quicklisp installer.
WORKDIR /usr/local/share/common-lisp/source/quicklisp/

ENV QUICKLISP_SIGNING_KEY D7A3489DDEFE32B7D0E7CC61307965AB028B5FF7

RUN set -x \
    && curl -fsSL "https://beta.quicklisp.org/quicklisp.lisp" > quicklisp.lisp \
    && curl -fsSL "https://beta.quicklisp.org/quicklisp.lisp.asc" > quicklisp.lisp.asc \
    && GNUPGHOME="$(mktemp -d)" \
    && export GNUPGHOME \
    && gpg --batch --keyserver ha.pool.sks-keyservers.net --recv-keys "${QUICKLISP_SIGNING_KEY}" \
    && gpg --batch --verify "quicklisp.lisp.asc" "quicklisp.lisp" \
    && rm quicklisp.lisp.asc \
    && rm -rf "$GNUPGHOME"
