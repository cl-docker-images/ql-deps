#!/usr/bin/env bash
set -Eeuo pipefail

cd "$(dirname "$(readlink -f "$BASH_SOURCE")")"

versions=( "$@" )

generated_warning() {
    cat <<EOH
#
# NOTE: THIS DOCKERFILE IS GENERATED VIA "update.sh"
#
# PLEASE DO NOT EDIT IT DIRECTLY.
#
EOH
}

for version in "${versions[@]}"; do

    for v in \
        buster/ \
    ; do
        os="${v%%/*}"
        dir="$version/$v"

        mkdir -p "$dir"

        case "$os" in
            buster|stretch)
                template="apt"
                from="buildpack-deps:$os"
                ;;
        esac

        template="Dockerfile-${template}.template"

        { generated_warning; cat "$template"; } > "$dir/Dockerfile"

        sed -ri \
            -e 's,^(FROM) .*,\1 '"$from"',' \
            "$dir/Dockerfile"
    done
done
